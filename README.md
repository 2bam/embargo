embargo
=======
**Control Fidel's behavior through external interfacing.**

For [Fidel Dungeon Rescue](http://store.steampowered.com/app/573170/Fidel_Dungeon_Rescue/) bot making.

First install Python 3.4+ and it's dependencies using `pip`:
```
pip install keyboard
pip install clipboard
```

Then just `import embargo` and use it's two simple classes

Example
```
from embargo import *
import time

dungeon = Dungeon()
dungeon.update()
print(dungeon.fidel_at)
for k in ['width', 'height']:
    print(dungeon.settings[k])

fx, fy = dungeon.fidel_at
print(dungeon.matrix[dungeon.fidel_at])
print(dungeon.matrix[fx+1, fy])

fidel = Fidel(dungeon)

time.sleep(1)
fidel.move(1, 0)
fidel.move(0, 1)

time.sleep(1)
fidel.bark()

time.sleep(1)
fidel.undo_move()
fidel.undo_move()

fidel.use_item(1)
fidel.use_item(2)

time.sleep(1)
for i in range(6):
    fidel.move(1, 0)
fidel.enter()
```

Tested on Python 3.4 - Windows only
