# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# embargo: Control Fidel's behavior through external interfacing  #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Dungeon class interfaces with the game.
#	.settings		: Global settings dict
#	.matrix[x, y]	: Each cell is a list of dicts with properties.
#                     There can be multiple properties (e.g. Ernesto_Doll and Portal) on the same cell.
#                     Each dict ENTITY key is the entity type (fidel, spider, tortoise...)
#                       and the rest are components like "hp", "gold", etc.
#	.fidel_at		: Fidel's (x, y) position
#
# Fidel class controls fidel.
#	.move(), .bark(), use_item(), .enter(), .undo_move()
#
# See the bottom of the file for an example.

import clipboard	# pip install clipboard
import keyboard		# pip install keyboard
import re
import time
from collections import defaultdict
from ctypes import windll

WND_TITLE	= 'Fidel Dungeon Rescue'
DELAY_FOCUS = 0.1		#Un-pause
DELAY_PR	= 0.1

FIDEL = 'fidel'
ENTITY = 'entity'

class Dungeon:
	def __init__(self):
		self.leave_game_focused = True
		self.dirty = True
		self.fidel_at = (-1, -1)

		self.hwnd = windll.user32.FindWindowW(None, WND_TITLE)
		if not self.hwnd:
			raise Exception('Window not found')

	def press(self, code):
		self.dirty = True
		self._press(code)

	def _press(self, code):
		old = windll.user32.GetForegroundWindow()
		if old != self.hwnd:
			windll.user32.SetForegroundWindow(self.hwnd)
			time.sleep(DELAY_FOCUS)
		keyboard.press(code)
		time.sleep(DELAY_PR)
		keyboard.release(code)
		if not self.leave_game_focused and old != self.hwnd:
			try:
				windll.user32.SetForegroundWindow(old)
				time.sleep(DELAY_FOCUS)
			except:
				pass

	def _process(self, s: str):
		self.settings = {}
		self.matrix = defaultdict(list)
		for line in re.split(r'[\r?\n?]+', s):
			line = line.strip()
			if not line or line[0] == '#':
				continue

			sett = re.match(r'^(\w+):(.*)$', line)
			cell = re.match(r'^(\d+),(\d+)\|(.*)$', line)

			if sett:
				k, v = sett.groups()
				self.settings[k] = v
			elif cell:
				x, y, comps = cell.groups()
				x = int(x)
				y = int(y)
				cd = dict()
				self.matrix[x, y].append(cd)
				if comps:
					comps = comps.split('|')
					entity = comps[0]
					cd[ENTITY] = entity
					if entity == FIDEL:
						self.fidel_at = (x, y)
					for kv in comps[1:]:
						k, v = kv.split(':')
						cd[k] = v
			else:
				raise Exception('Invalid format line: "' + line + '"')

	def update(self, force = False):
		if not (self.dirty or force):
			return

		self._press('f2')
		#self._press('ctrl+c')
		# WM_COPY = 0x0301
		# win32gui.SendMessage(self.hwndMain, win32con.WM_COPY, 0, 0)

		cs = clipboard.paste()
		if isinstance(cs, str):
			self._process(cs)

		self.dirty = False


class Fidel:
	def __init__(self, dungeon):
		self.dungeon = dungeon

	def undo_move(self):
		self.dungeon.press('z')

	def move(self, x, y):
		assert x in [-1, 1] and y == 0 or y in [-1, 1] and x == 0
		if x == -1:
			self.dungeon.press('a')
		elif x == 1:
			self.dungeon.press('d')
		elif y == -1:
			self.dungeon.press('w')
		elif y == 1:
			self.dungeon.press('s')

	def use_item(self, index):
		self.dungeon.press(chr(ord('0') + index))

	def bark(self):
		self.dungeon.press('b')

	def swap(self):
		self.enter()

	def enter(self):
		self.dungeon.press('space')


if __name__ == '__main__':
	dungeon = Dungeon()
	dungeon.update()

	for k in ['width', 'height']:
		print(f'{k} = {dungeon.settings[k]}')

	fx, fy = dungeon.fidel_at

	# Show fidel position
	print(dungeon.fidel_at)
	# Show all entities at fidel's position
	print(' -- '.join(map(str, dungeon.matrix[dungeon.fidel_at])))
	# Show all entities to the right of fidel's position
	print(' -- '.join(map(str, dungeon.matrix[fx+1, fy])))

	fidel = Fidel(dungeon)

	time.sleep(1)
	fidel.move(1, 0)
	fidel.move(0, 1)

	time.sleep(1)
	fidel.bark()

	time.sleep(1)
	fidel.undo_move()
	fidel.undo_move()

	fidel.use_item(1)
	fidel.use_item(2)

	time.sleep(1)
	for i in range(6):
		fidel.move(1, 0)

	fidel.enter()

